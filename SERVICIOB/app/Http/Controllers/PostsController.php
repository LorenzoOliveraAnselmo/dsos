<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use  GuzzleHttp\Client;

class PostsController extends Controller
{
    public function index() //metodo que recoge el archivo y lo decodifica a json para poder mostrarlo
    {
    	$client  =  new  Client ([ 
	    // El URI base se usa con solicitudes relativas 
	    'base_uri'  =>  'https://jsonplaceholder.typicode.com' , //direccion de donde provienen los datos
	    // Puede establecer cualquier número de opciones de solicitud predeterminadas. 
	    'timeout'   =>  2.0 , 
		]);

		$response = $client->request('GET', 'posts'); //recoge todos los post

		$posts = json_decode($response->getBody()->getContents());

	    return view('posts.index', compact('posts'));
    }

    public function show($id) //metodo que recoge el archivo y lo decodifica a json para poder mostrarlo mediante su id
    {
    	$client  =  new  Client ([ 
	    // El URI base se usa con solicitudes relativas 
	    'base_uri'  =>  'https://jsonplaceholder.typicode.com' , //direccion de donde provienen los datos
	    // Puede establecer cualquier número de opciones de solicitud predeterminadas. 
	    'timeout'   =>  2.0 , 
		]);

		$response = $client->request('GET', "posts/{$id}"); //recoge solo el post del id proporcionado

		$post = json_decode($response->getBody()->getContents());

	    return view('posts.show', compact('post'));
    }
}
